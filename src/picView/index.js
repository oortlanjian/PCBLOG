import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import Viewer from 'react-viewer';
import 'react-viewer/dist/index.css';
import Loading from '../home/components/Loading'

const Electron = window.electron;

class PicView extends Component {
  constructor(props){
    super(props);
    this.state = {
      picDataInfo:null
    }
  }

  componentDidMount(){
    //监听主进程发送过来的数据
    Electron.ipcRenderer.on('openPic',(event, index, data)=>{
        this.setState({
          picDataInfo:data,
          activeIndex:index
        });

        console.log(88888,index,data);
    });
  }

  render() {
    let imagesArr = [];
    let htmlS = "";
    if(!!this.state.picDataInfo){
      for(let i = 0; i < this.state.picDataInfo.length;i++){
        if( this.state.picDataInfo[i].type === 1){
          let tempObj = {};
          tempObj.src =  this.state.picDataInfo[i].pictureUrl;
          tempObj.downloadUrl = this.state.picDataInfo[i].pictureUrl;
          tempObj.alt = "图片";
          imagesArr.push(tempObj);
        }
      }
      htmlS = <Viewer
          visible={true}
          onClose={() => { this.setState({ visible: false }); } }
          images={imagesArr}
          activeIndex={this.state.activeIndex}
          onClose={this.closePicWin}
          downloadable={true}
          />
    }else{
      htmlS = <Loading/>
    }

    return (
      <div className="PictureViewC">
       {htmlS}
      </div>
    );
  }

  //关闭
  closePicWin(){
    Electron.ipcRenderer.send("closePicWindow");
  }
}
ReactDOM.render(<PicView />, document.getElementById('root'));
