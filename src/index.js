/*
 * @Author: lanjian
 * @Date: 2018-09-18 09:58:29
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 12:10:28
 * @Content desc:  多页面主入口页
*/

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">PCBlog</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <a href="login/index.html">pageF</a>
        <div>
          <a href="picView/index.html">pageS</a>
        </div>
        <div>
          <a href="home/">pageS</a>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
