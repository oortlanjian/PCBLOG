/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:04:19
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-14 20:52:32
 * @Content desc:左边布局类
 */

import React, { Component } from 'react';
import BlogList  from './BlogList';

class BlogListContent extends Component{

    render(){
        return (
            <div className="right">
            <div className="Rtitle"> {this.props.rData.disPlaytitle}</div>
                <BlogList rData={this.props.rData}/>
            </div>
        )
    }

}

export default BlogListContent;