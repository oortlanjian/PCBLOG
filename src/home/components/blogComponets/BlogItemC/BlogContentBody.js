/*
 * @Author: lanjian
 * @Date: 2018-09-11 19:31:07
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-11 19:31:4111
  * @Content desc: 动态主体内容组件
 */

import React,{Component} from 'react';
import classNames from 'classnames';

 export default class BlogContentBody extends Component{

    constructor(props){
        super(props);
        this.state = {
            itemData:this.props.data,
            contentOpen:(!!this.props.data.isDetail)?true:false,
            isDetail:(!!this.props.data.isDetail)?true:false
        }
        this.openOrCloseContent = this.openOrCloseContent.bind(this);
    }

    //父组件更新props的变化
    componentWillReceiveProps(nextProps){
        this.setState ({
            itemData:nextProps.data
        })
    }

    render(){
        let html = {__html:this.dealwithContent()};
        //判断是否要显示全文的标记，暂时三百给字符
        let openOrCloseHtml = "";
        let htmlS = this.state.itemData.blogContent;
        htmlS = htmlS.replace(/<[^>]+>/g,"");
        if(htmlS.length > 300 && !this.state.isDetail){
            openOrCloseHtml = <span className={classNames({
                "textBlue":true,
                "openOrCloseContent":true
            })} onClick={this.openOrCloseContent}>{(this.state.contentOpen)?"收起":"全文"}</span>
        }
        return (
            <div className="blogContentBody">
                <div className="blogTitle">{this.state.itemData.blogTitle}</div>
                <div ref="blogContent" className={classNames({
                    "blogContent":true,
                    "blogContentClose":!this.state.contentOpen
                })} dangerouslySetInnerHTML={html} ></div>
                {openOrCloseHtml}
            </div>
        )
    }

    componentDidMount(){
        // let obj = this.refs.blogContent.style;
        // console.log("打印111----",obj);
    }

    openOrCloseContent(){
        this.setState({
            contentOpen:!this.state.contentOpen
        })
    }

    //处理内容文本
    dealwithContent(){
        let data = this.state.itemData;
        ///处理营养热帖下换行（主页针对品高数据）
        data.blogContent =  data.blogContent.replace(/[\n\r]/g, '</br>');
        data.blogContent =  data.blogContent.replace(/[\s|\?]/g,"&nbsp;");
        data.blogContent = this.dealWithAtPersonOrDept(data);
        data.blogContent = this.dealWithTopic(data);
        data.blogContent = this.dealWithMeip(data);
        return data.blogContent;
    }
    //处理动态内容区域@ren
    dealWithAtPersonOrDept(data,isNotSpan){
        if(!data.atUsers){
            data.atUsers = [];;
        }
        if(!data.atDepts){
            data.atDepts = [];
        }
        var tepmContent = data.blogContent;
        var atPersonArr = data.atUsers;
        var atDept = data.atDepts;
        var personTepArr = [];
        var deptTepArr = [];
        for(let i = 0; i< atPersonArr.length; i++){
            if(!!isNotSpan){
                personTepArr.push("@"+ atPersonArr[i].userName);
            }else{
                personTepArr.push("<span>@"+ atPersonArr[i].userName +"</span>");
            }
        }
        for(let i = 0; i< personTepArr.length; i++){
            tepmContent =  tepmContent.replace(personTepArr[i],"~%"+i);
        }
        for(let i = 0 ; i< atPersonArr.length; i++){
            let sstr = '<span class ="atFontSize textBlue atPersonClass" data-userId='+ atPersonArr[i].userId +'>@'+ atPersonArr[i].userName +'</span>';
            // if(atPersonArr[i].userId == directorUserId){
            //     sstr = '<span class ="atFontSize textRed atPersonClass" data-userId='+ atPersonArr[i].userId +'>@'+ atPersonArr[i].userName +'</span>';
            // }
            tepmContent = tepmContent.replace("~%"+i,sstr);

        }
        //处理部门
        for(let i = 0; i< atDept.length; i++){
            deptTepArr.push("<span>@"+ atDept[i].deptName +"</span>");
        }
        for(let i = 0; i< deptTepArr.length; i++){
            tepmContent =  tepmContent.replace(deptTepArr[i],"$%"+i);
        }
        for(let i = 0 ; i< atDept.length; i++){
            let sstr = '<span class ="atFontSize textBlue" data-userId='+ atDept[i].deptId +'>@'+ atDept[i].deptName +'</span>';
            tepmContent = tepmContent.replace("$%"+i,sstr);
        }
        //去除最后一个是逗号
        if(!!tepmContent){
            (tepmContent[tepmContent.length-1] === "," )?(tepmContent = tepmContent.substr(0,tepmContent.length-1)):tepmContent;
        }
        return tepmContent;
    }

    //处理话题
    dealWithTopic(data){
        let topicName = "";
        let contentS = data.blogContent;
        if(!!data.topic && data.topic.length!==0){
            for(let jj = 0 ; jj < data.topic.length; jj++){
                topicName += '<span class="atFontSize textBlue topicClass" data-topicId = '+data.topic[jj].topicId+'>#'+data.topic[jj].topicName+'#</span>';
            }
            let topicArr= [];
            for(let i = 0; i < data.topic.length; i++){
                topicArr.push("#"+ data.topic[i].topicName + "#");
            }
            for(let i = 0; i< topicArr.length; i++){
                contentS =  contentS.replace(topicArr[i],"");
            }
        }
        //话题加载内容前面
        contentS = topicName + contentS;
        return contentS;
    }

    //处理美篇风格的
    dealWithMeip(data){
        for(let tt=0 ; tt < 9;tt++){
            if(tt === 0){
                data.blogContent = data.blogContent.replace('%'+tt+'%',"");
            }else{
                data.blogContent = data.blogContent.replace('%'+tt+'%',"</br>");//显示一段换行
            }
            data.blogContent = data.blogContent.replace("%END%","");
        }
        //处理空的段落
        let contentStrArr = data.blogContent.split("</br>")
        let newContentStrArr = [];
        for(let xxx = 0; xxx <contentStrArr.length ;xxx++){
            if(!!contentStrArr[xxx]){
                newContentStrArr.push(contentStrArr[xxx]);
            }
        }
        data.blogContent = newContentStrArr.join("</br>");
        return data.blogContent;
    }
}
