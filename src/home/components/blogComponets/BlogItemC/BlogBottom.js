/*
 * @Author: lanjian
 * @Date: 2018-09-11 19:31:07
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-11 19:31:4111
  * @Content desc: 动态底部组件
 */

import React,{Component} from 'react';
import {Icon} from 'antd';
import _ from 'lodash';
import classNames from 'classnames';

export default class BlogBottom extends Component{
    constructor(props){
        super(props);
        this.state = {
            itemData:this.props.data,
            isThumpUp:this.props.data.isThumpUp,
            collect:this.props.data.collect,
            thumpUpCount:this.props.data.thumpUpCount,
            commentCount:this.props.data.commentCount
        }
        this.collectBlog = this.collectBlog.bind(this);
        this.forwardtBlog = this.forwardtBlog.bind(this);
        this.commentBlog = this.commentBlog.bind(this);
        this.thumpBlog = this.thumpBlog.bind(this);
    }

    //父组件更新props的变化
    componentWillReceiveProps(nextProps){
        this.setState ({
            itemData:nextProps.data,
            isThumpUp:nextProps.data.isThumpUp,
            collect:nextProps.data.collect,
            thumpUpCount:nextProps.data.thumpUpCount,
            commentCount:nextProps.data.commentCount
        })
    }


    render(){
        let loctionMsg = "";
        if(!!this.state.itemData && !!this.state.itemData.address){
            loctionMsg = <div className="location">
                    <Icon type="environment" theme="outlined" />
                    {this.state.itemData.address}
                </div>
        }
        let timeStr = this.calcTime(this.state.itemData.createTime);
        let whoCanSee = "";
        if(!!this.state.itemData && this.state.itemData.msgStatus === 3){
            loctionMsg = <div className="bottomEye">
                    <Icon type="eye" theme="outlined" />部分可见
                </div>
        }
        return (
            <div className="blogBottom">
                <div className="timeOrLocation">
                    <div className="bottomTime">{timeStr}</div>
                    {loctionMsg}
                    {whoCanSee}
                </div>
                <div className="bottomOpr">
                    <div className="collectDiv" onClick={this.collectBlog}>
                        <a className={classNames({
                             "aDiv":true,
                             "aactive":this.state.collect
                        })}><Icon type="star" theme={this.state.collect?"filled":"outlined" }/>{this.state.collect?"已收藏":"收藏"}</a>
                    </div>
                    <div className="forwardDiv" onClick={this.forwardtBlog}><a className="aDiv"><Icon type="export" theme="outlined" />转发</a></div>
                    <div className="commentDiv" onClick={this.commentBlog}><a className="aDiv"><Icon type="form" theme="outlined" />{this.state.commentCount}</a></div>
                    <div className="thumpDiv" onClick={this.thumpBlog}>
                        <a className={classNames({
                            "aDiv":true,
                            "aactive":this.state.isThumpUp
                        })}><Icon type="like" theme={this.state.isThumpUp?"filled":"outlined" } />{this.state.thumpUpCount}</a>
                        </div>
                </div>
            </div>
        )
    }

    collectBlog(){
        this.setState({
            collect:!this.state.collect,
        });
    }

    forwardtBlog(){

    }

    commentBlog(){

    }

    thumpBlog(){
        let count = this.state.thumpUpCount;
        if(this.state.isThumpUp){
            count--;
        }else{
            count++;
        }
        this.setState({
            isThumpUp:!this.state.isThumpUp,
            thumpUpCount:count
        });
    }

    calcTime(str){
        // tameStamp 格式化
        //将字符串转换成时间格式
        let timePublish = new Date(str);
        let timeNow = new Date();
        let minute = 1000 * 60;
        let hour = minute * 60;
        let day = hour * 24;
        let month = day * 30;
        let diffValue = timeNow - timePublish;
        let diffMonth = diffValue / month;
        let diffWeek = diffValue / (7 * day);
        let diffDay = diffValue / day;
        let diffHour = diffValue / hour;
        let diffMinute = diffValue / minute;
        let result ="";
        if (diffValue < 0) {
            result = "刚刚发表";
        }
        else if (diffMonth > 3) {
            result = timePublish.getFullYear()+"-";
            result += timePublish.getMonth() + 1 + "-";
            result += timePublish.getDate();
        }
        else if (diffMonth > 1) {
            result = _.parseInt(diffMonth) + "月前";
        }
        else if (diffWeek > 1) {
            result = _.parseInt(diffWeek) + "周前";
        }
        else if (diffDay > 1) {
            result = _.parseInt(diffDay) + "天前";
        }
        else if (diffHour > 1) {
            result = _.parseInt(diffHour) + "小时前";
        }
        else if (diffMinute > 1) {
            result = _.parseInt(diffMinute) + "分钟前";
        }
        else {
            result = "刚刚发表";
        }
        return result;
    }
}
