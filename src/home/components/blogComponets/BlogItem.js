/*
 * @Author: lanjian
 * @Date: 2018-09-11 16:45:32
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-17 18:15:54
 * @Content desc: 动态组件类
 */
import React, {Component} from 'react';
import BlogHeader from './BlogItemC/BlogHeader';
import BlogContentBody from './BlogItemC/BlogContentBody';
import BlogImageBody from './BlogItemC/BlogImageBody';
import BlogBottom from './BlogItemC/BlogBottom';
import BlogThumps from './BlogItemC/BlogThumps';
import BlogComments from './BlogItemC/BlogComments';

class BlogItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            itemData:this.props.data
        }
        console.log(this.props.data);
    }

    render(){
        return (
            <div className="blogItem">
                <BlogHeader data={this.props.data}/>
                <BlogContentBody data={this.props.data}/>
                {/* {(this.props.data.blogAttachments ===0)?"":} */}
                <BlogImageBody data={this.props.data}/>
                <BlogBottom data={this.props.data}/>
                <BlogThumps data={this.props.data}/>
                <BlogComments data={this.props.data}/>
            </div>
        )
    }
}

export default BlogItem;