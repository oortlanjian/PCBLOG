/*
 * @Author: lanjian
 * @Date: 2018-09-11 11:02:04
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 17:42:05
  * @Content desc:  动态列表  上拉加载更多，下拉刷新
 */
import React ,{Component} from 'react';
import _ from 'lodash';
import BlogItem from './BlogItem';
import {netUtil} from '../../../utils/index';
import {uiConfig} from '../../../config/index';
import {Icon} from 'antd';

class BlogList extends Component{
    constructor(props){
        super(props);
        this.state={
            hasMore:true,
            autoLoadMore:5,
            initializing:0,
            dataList:[]
        }
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleLoadMore = this.handleLoadMore.bind(this);
        this.scrollEvent = this.scrollEvent.bind(this);
        this.getData(this.props.rData);
    }

    componentDidMount(){
        // this.getData(this.props.rData);
    }

    componentWillReceiveProps(nextProps){
        //重新置空列表
        this.setState({
            hasMore:true,
            dataList:[]
        });
        this.getData(nextProps.rData);
    }

    getData(tempProp,loadMore){
        let params = {
            userId: "3dfcbe3d-8a5d-47fc-9dba-7c8af8d7d50e",
            blogPageSize:10,
            lastBlogId:"",
            lastBlogUpdateTime:"",
            isValidate: false
        };
        //拉取上一次的参数
        if(this.state.dataList.length > 0){
            let length = this.state.dataList.length;
            params.lastBlogId = this.state.dataList[length-1].blogId;
            params.lastBlogUpdateTime = this.state.dataList[length-1].lastUpdateTime;
        }
        var userData = window.userData;
        if(!!userData && tempProp.index === 3){//二级单位
            let pathNum = userData.dept.path.split('/');
            let tempArr = _.slice(pathNum, 3, pathNum.length)
            if(tempArr.length > 0){
                params.orgId = [tempArr[0]]
            }
        }
        else if(!!userData &&tempProp.index === 4){//三级单位
            let pathNum = userData.dept.path.split('/');
            let tempArr = _.slice(pathNum, 4, pathNum.length-1)
            if(tempArr.length > 0){
                params.orgId = tempArr;
            }
        }else if(tempProp.index === 7){//话题
            console.log("话题");
        }
        netUtil.fetchPost("/moments/ui/"+uiConfig.blogSortList[tempProp.index-1].interface,
            params,
            (res)=>{
                if(res.code === 200){
                    this.setState({
                        dataList:_.concat(this.state.dataList,res.data.blogs),
                        hasMore:!res.data.isLast
                    });
                }
        })
    }

    render(){
        console.log("BlogList",this.state.dataList);
        const listItems = this.state.dataList.map((item,index) =>{
            return (<BlogItem key={index} data={item}></BlogItem>);
        });

        let loadMoreHtml =<div className="loading"><Icon type="loading" theme="outlined" /> 加载中...</div>;
        if(!this.state.hasMore){
            loadMoreHtml = <div className="notMore">没有更多了!</div>;
        }
        return (
            <div onScroll={this.scrollEvent} refs="listView" className="blogListCotainer">
               {listItems}
               {loadMoreHtml}
            </div>
        );
    }

    componentDidMount(){
        console.log();
    }

    scrollEvent(event){
        const clientHeight = event.target.clientHeight
        const scrollHeight = event.target.scrollHeight
        const scrollTop = event.target.scrollTop
        const isBottom = (clientHeight + scrollTop === scrollHeight)
        console.log('is bottom:' + isBottom)
        if(isBottom && this.state.hasMore){
            this.getData(this.props.rData,true);
        }
    }

    //处理刷新
    handleRefresh(){
        console.log("handleRefresh");
    }

    //处理加载更多
    handleLoadMore(){
    }
}

export default BlogList;