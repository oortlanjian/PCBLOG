/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:00:57
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-07 10:01:244
 * @Content desc:  中间布局 动态tab）
 */

import React ,{Component} from 'react';
import BlogSortList from './BlogSortList';

class BlogBar extends Component{

    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return(
            <div className="center">
                <BlogSortList sortListClick={this.props.sortListClick}></BlogSortList>
            </div>
        );
    }
}

export default BlogBar;