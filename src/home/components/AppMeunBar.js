/*
 * @Author: lanjian
 * @Date: 2018-09-10 17:13:59
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-10 17:14:299
  * @Content desc: 应用的菜单栏目
 */
import React, {Component} from 'react';
import {Icon,message} from 'antd';
import classnames from 'classnames';
import {uiConfig} from '../../config/index';
const Electron = window.electron;

class AppMeunBar extends Component{

    constructor(props){
        super(props);
        this.state={
            isFullScreen:false,
            isAlwaysOnTop:false
        }
        this.hideWindow = this.hideWindow.bind(this);
        this.closeWindow = this.closeWindow.bind(this);
        this.fullscreenOrNot = this.fullscreenOrNot.bind(this);
        this.alwaysOnTop = this.alwaysOnTop.bind(this);
    }
    render(){
        let minOrMaxOpr = <Icon type="border" theme="outlined" className="meunTopIcon" onClick ={this.fullscreenOrNot}/>;
        if(this.state.isFullScreen){
            minOrMaxOpr = <Icon  type="switcher" theme="outlined" className="meunTopIcon" onClick ={this.fullscreenOrNot}/>;
        }
        return (
            <div className="appMeunBarDiv">
                <Icon  type="pushpin" theme="outlined"
                    className={classnames({
                        "meunTopIcon":true,
                        "alwayOnTopaActive":this.state.isAlwaysOnTop})} onClick ={this.alwaysOnTop}/>
                <Icon  type="minus" theme="outlined" className="meunTopIcon" onClick ={this.hideWindow}/>
                    {minOrMaxOpr}
                <Icon  type="close" theme="outlined" className="meunTopIcon" onClick ={this.closeWindow}/>
            </div>
        );

    }

    hideWindow(){
        Electron.ipcRenderer.send("hideapp");
    }

    closeWindow(){
        Electron.ipcRenderer.send("exitApp");
    }

    fullscreenOrNot(){
        this.setState({
            isFullScreen: !this.state.isFullScreen
          })
        Electron.ipcRenderer.send("maximize");
    }

    alwaysOnTop(){
        this.setState({
            isAlwaysOnTop: !this.state.isAlwaysOnTop
          })
        if(!this.state.isAlwaysOnTop){
            message.info(uiConfig.alwaysOnTopTips);
        }
        Electron.ipcRenderer.send("alwaysOnTop");
    }
}

export default AppMeunBar;