/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:01:30
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 20:05:51
 * @Content desc:app整体layout布局 路由主页
 */

import React, { Component } from 'react';
import AppBar from './AppBar';
import BlogHome from './BlogHome';
import AppMeunBar from '../components/AppMeunBar';
import CollectionHome from './CollectionHome';
import MessageHome from './MessageHome';
import BlogDetail from '../container/BlogDetail';
import {Switch,Route} from 'react-router-dom';

class Layout extends Component {

  constructor(props){
      super(props);
      this.state = {
        cData:{
          whichBarClick:"blog",
        }
      }
      this.barClick = this.barClick.bind(this);
  }

  render() {

      return (
          <div className="parent">
            <AppBar barClick={this.barClick}/>
            <div className="layoutContainer">
              <AppMeunBar/>
                <Switch>
                  <Route exact path='/home' component={BlogHome}/>
                  <Route path='/home/blogdetail/:blogId' component={BlogDetail}/>
                  <Route path='/home/collection' component={CollectionHome}/>
                  <Route path='/home/message' component={MessageHome}/>
                </Switch>
            </div>
        </div>
      );
  }

  barClick(str){
    this.setState({
      cData:{
        whichBarClick:str,
      }
    });
  }
}

export default Layout;
