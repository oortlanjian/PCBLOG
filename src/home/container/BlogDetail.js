/*
 * @Author: lanjian
 * @Date: 2018-09-17 14:21:20
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 17:50:20
 * @Content desc:  动态详情
 */

 import React ,{Component} from 'react';
 import {Link} from 'react-router-dom';
 import {Icon} from 'antd';
 import { netUtil } from '../../utils/index';
 import Loading from '../components/Loading';
import BlogItem from '../components/blogComponets/BlogItem';

 class BlogDetail extends Component{

    constructor(props){
        super(props);
        this.state={
            blogId:this.props.match.params.blogId,
            blogInfo:null
        }
    }

    componentDidMount(){
        netUtil.fetchPost("/moments/blog/ui/getBlogInfo",
            {
                // userId: window.userData.userId,
                userId: "3dfcbe3d-8a5d-47fc-9dba-7c8af8d7d50e",
                blogId: this.state.blogId
            },
            (res)=>{
                console.log("数据",res);
                //添加是详情的bligItem
                let blogInfo = res.data;
                blogInfo.isDetail = true;
                this.setState({
                    blogInfo:blogInfo
                });
        })
    }

    render(){
        let detailCStr = "";
        if(!!this.state.blogInfo){
            detailCStr = <BlogItem data={this.state.blogInfo}/>
        }else{
            detailCStr = <Loading/>;
        }

        return (
            <div className="blogDetail">
                <div className="topBar">
                    <Link to="/home"><Icon type="left" theme="outlined" />返回</Link>
                    <div className="detailTitle">详情</div>
                </div>
                <div className="detailContent">
                    {detailCStr}
                </div>
            </div>
        );
    }
 }

 export default BlogDetail;