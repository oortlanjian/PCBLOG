/*
 * @Author: lanjian
 * @Date: 2018-09-07 17:03:53
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-14 19:21:25
  * @Content desc:  工具类总入口
 */
import netUtil from './netUtil';
import store from './store';

export {
    netUtil,
    store
}
