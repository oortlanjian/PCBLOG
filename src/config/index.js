/*
 * @Author: lanjian
 * @Date: 2018-09-07 18:01:15
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-10 11:08:38
  * @Content desc: 配置入口模块
 */

import netConfig from './netconfig';
import uiConfig from './uiConfig';
export {
    netConfig,
    uiConfig
}
