/*
 * @Author: lanjian
 * @Date: 2018-09-07 16:55:03
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-13 15:10:06
  * @Content desc:  网络配置文件配置文件
 */

const config = {
    dev:false,
    testUiServiceUrlPre:"http://20.97.6.109:3001/momentsuiservice/v1",
    officeUiServiceUrlPre:"http://20.97.6.131:3001/momentsuiservice/v1",
    preHead:"http://172.29.3.76:9218"
};

export default config;