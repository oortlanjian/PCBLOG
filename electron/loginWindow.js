/*
 * @Author: lanjian
 * @Date: 2018-09-18 18:15:20
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 19:46:34
 * @Content desc:登录窗口
*/


const BrowserWindow = require('electron').BrowserWindow;
const path = require('path');
function loginWindowFunc(data){
    //作为主界面窗口的子窗口
    let login = new BrowserWindow({
        width: 450,
        height: 380,
        autoHideMenuBar: true,
        fullscreenable: false,
        webPreferences: {
            javascript: true,
            plugins: true,
            nodeIntegration: true,
            webSecurity: false,
            preload: path.join(__dirname, '../public/renderer.js') // 但预加载的 js 文件内仍可以使用 Nodejs 的 API
            },
        show: false,
        frame: false //全屏
    })
    // 打开开发者工具。
    login.webContents.openDevTools()
    login.loadURL('http://localhost:3000/login');
    login.once('ready-to-show', () => {
        login.show();
    });
    return login;
}

module.exports = loginWindowFunc;