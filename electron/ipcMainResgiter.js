/*
 * @Author: lanjian
 * @Date: 2018-09-07 10:12:15
 * @Last Modified by: lanjian
 * @Last Modified time: 2018-09-18 19:36:28
  * @Content desc:主进程ipc listener
 */

const {ipcMain} = require('electron');
const openPicWindowFunc = require('./openNewWindow');
function ipcMainResgiter(win){

  ipcMain.on('hideapp', function(e) {
    win.hide();
    e.sender.send('hided');
  });

  ipcMain.on('minimize', function(e) {
    win.minimize();
    e.sender.send('minimize');
  });


  ipcMain.on('exitApp', function(e) {
    win.minimize();
    e.sender.send('minimize');
  });

  ipcMain.on('maximize', function(e) {
    if (win.isMaximized()) {
        win.unmaximize();
    } else {
        win.maximize();
    }
    e.sender.send('maximize');
  });

  //置顶
  ipcMain.on('alwaysOnTop', function(e) {
    win.setAlwaysOnTop(!win.isAlwaysOnTop())
  });

  let picwin = null
  //打开一个图片浏览窗口
  ipcMain.on('openPicWindow', function(e,index,data) {
    //保证只有一个浏览图片窗口
    //窗口存在就只需要更新数据
    if(!!picwin){
      //通知事件
      picwin.webContents.send('openPic',index,data)
    }else{
      picwin = openPicWindowFunc(win,index,data);
    }
  });

  //打开一个图片浏览窗口
  ipcMain.on('closePicWindow', function(e,index,data) {
    if(!!picwin){
      picwin.close();
      picwin = null;
    }
  });
}

module.exports = {ipcMainResgiter};