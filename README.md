
## package.json配置

### electron 主进程入口
 "main":"main.js"

### electron 打包的入口文件

"homepage":"."

通过`npm run build`是通过`webpack`打包出来的`build`项目是绝对路径。在`electron`中很多资源都引用不到。添加该配置可以将路径改为相对路径

"DEV":true

这是项目开发环境，如果为true  mian.js中electron的入口为`http://localhost:3000/`。通过`electron .`运行项目时需要先`npm start`启动项目。然后`electron .`运行项目

"DEV":false

这是项目的发布环境。需要先`npm run build`打包项目。然后通过`electron-packager . app --darwin --out presenterTool --arch=x64 --overwrite --ignore=node_modulesls --electron-version=1.8.2`打包项目


### Create-react-app与Electron中线程间的通信
之前，将create-react-app与electron集成在了一个项目中。但是在React中无法使用electron。

当在React中使用require('electron')时就会报TypeError: fs.existsSync is not a function的错误。因为React中无法使用Node.js的模块.

### 解决方案
1，修改创建浏览器的入口代码,添加preload配置项。将renderer.js作为预加载文件
2，修改piblic/index.html文件，在<div id="root"></div>前引入renderer.js文件
3，在React组件中如下使用electron


###项目启动

  yarn start  启动react项目，编译，并启动服务
  yarn electron-start  启动electron
  yarn   electron-package  打包